const router = require("express").Router();
const bookController = require('../controllers/book_control.js')
const adminbookController = require('../controllers/admin/book_control')

// API users
// nampilin data di semua users
router.get('/api/users', bookController.getUsers)
// nampilin data sesuai user id
router.get('/api/users/:id', bookController.getUserById)
// buat user baru
router.post('/api/users', bookController.createUser)
// hapus user
router.delete('/api/users/:id', bookController.deleteUsers)


// API books
// nampilin data semua buku
router.get('/api/books', bookController.getBooks)
// nampilin data buku berdasarkan id
router.get('/api/books/:id', bookController.getBookById)
// update isi buku
router.put('/api/books/:id', bookController.updateBook)
// buat data buku
router.post('/api/books', bookController.createBook)
// menghapus data buku
router.delete('/api/books/:id', bookController.deleteBook)

// API Admin
// router.get('/',adminbookController.getBooks)
router.get('/', adminbookController.getUserId)
router.get('/addBook', adminbookController.createBook)
router.get('/viewBook/:id', adminbookController.viewBook)
router.get('/editBook/:id', adminbookController.editBookPage)
router.post('/updateBook/:id', adminbookController.editBook)
router.get("/hapus/:id", adminbookController.hapusBook);

router.put('/editBook/:id', adminbookController.editBook)


module.exports = router
